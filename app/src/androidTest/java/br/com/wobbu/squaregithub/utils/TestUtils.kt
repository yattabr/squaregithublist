package br.com.wobbu.squaregithub.utils

import com.google.gson.GsonBuilder
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class TestUtils {

    fun <T> loadJson(path: String, clazz: Class<T>): T {
        try {
            val json = getFileString(path)
            val gson = GsonBuilder().serializeNulls().create()
            return gson.fromJson(json, clazz)
        } catch (e: IOException) {
            throw IllegalArgumentException("Could not deserialize: $path into class: $clazz")
        }

    }

    fun getFileString(path: String): String {
        try {

            val sb = StringBuilder()
            val reader = BufferedReader(
                InputStreamReader(
                    javaClass.classLoader!!.getResourceAsStream(path)
                )
            )
            var line = reader.readLine()
            while (line != null) {
                sb.append(line)
                line = reader.readLine()
            }
            return sb.toString()
        } catch (e: IOException) {
            throw IllegalArgumentException("Could not read from resource at: $path")
        }
    }
}