package br.com.wobbu.squaregithub

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import br.com.wobbu.squaregithub.main.MainActivity
import br.com.wobbu.squaregithub.utils.MockServerDispatcher
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityEspressoTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java, false, false)
    private lateinit var webServer: MockWebServer

    @Before
    @Throws(Exception::class)
    fun setup() {
        webServer = MockWebServer()
        webServer.start(8080)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        webServer.shutdown()
    }

    @Test
    fun fetchListOfRepositories() {
        webServer.setDispatcher(MockServerDispatcher.RequestDispatcher())
        activityRule.launchActivity(Intent())

        onView(withId(R.id.recycler_view)).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.txt_name), withText("yajl-objc"))).check(matches(isDisplayed()))
    }

    @Test
    fun searchRepositories() {
        webServer.setDispatcher(MockServerDispatcher.RequestDispatcher())
        activityRule.launchActivity(Intent())

        onView(withId(R.id.edit_search)).perform(clearText(), typeText("yajl"))

        onView(allOf(withId(R.id.txt_name), withText("yajl-objc"))).check(matches(isDisplayed()))
        onView(
            allOf(
                withId(R.id.txt_description),
                withText("Objective-C bindings for YAJL (Yet Another JSON Library) C library")
            )
        ).check(matches(isDisplayed()))
    }
}
