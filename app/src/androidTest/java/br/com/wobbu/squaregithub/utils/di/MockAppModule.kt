package br.com.wobbu.squaregithub.utils.di

import android.content.Context
import br.com.wobbu.squaregithub.di.AppModule
import br.com.wobbu.squaregithub.main.RepositoriesAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class MockAppModule(private val context: Context) : AppModule(context) {

    @Provides
    @Singleton
    override fun provideContext(): Context {
        return context
    }

    @Provides
    override fun usersAdapter(): RepositoriesAdapter {
        return RepositoriesAdapter(context)
    }
}