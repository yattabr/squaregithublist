package br.com.wobbu.squaregithub.utils

import br.com.wobbu.squaregithub.base.BaseApplication
import br.com.wobbu.squaregithub.di.AppComponent
import br.com.wobbu.squaregithub.di.DaggerAppComponent
import br.com.wobbu.squaregithub.utils.di.MockAppModule
import br.com.wobbu.squaregithub.utils.di.MockUtilsModule

class UiTestApplication : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun getAppComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(MockAppModule(this))
            .utilsModule(MockUtilsModule())
            .build()
    }

}