package br.com.wobbu.squaregithub.data

import com.google.gson.JsonElement
import io.reactivex.annotations.NonNull

class ApiResponse {
    var status: Status
    var data: JsonElement?
    var error: Throwable?

    constructor(status: Status, data: JsonElement?, error: Throwable?) {
        this.status = status
        this.data = data
        this.error = error
    }

    companion object {
        fun loading(): ApiResponse {
            return ApiResponse(Status.LOADING, null, null)
        }

        fun success(@NonNull data: JsonElement): ApiResponse {
            return ApiResponse(Status.SUCCESS, data, null)
        }

        fun error(@NonNull error: Throwable): ApiResponse {
            return ApiResponse(Status.ERROR, null, error)
        }
    }
}
