package br.com.wobbu.squaregithub.model

class Repositories {
    var name: String = ""
    var description: String = ""
    var owner: Owner = Owner()
}

class Owner {
    var avatar_url: String = ""
}