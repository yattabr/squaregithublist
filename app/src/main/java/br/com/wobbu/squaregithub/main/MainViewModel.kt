package br.com.wobbu.squaregithub.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.wobbu.squaregithub.data.ApiResponse
import br.com.wobbu.squaregithub.data.Repository
import br.com.wobbu.squaregithub.model.Repositories
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(var repository: Repository) : ViewModel() {

    val fetchRepositoriesObserver = MutableLiveData<Any>()
    private val disposables = CompositeDisposable()

    fun fetchRepositories() {
        disposables.add(repository.fetchRepositories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { fetchRepositoriesObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> fetchRepositoriesObserver.setValue(ApiResponse.success(result)) },
                { throwable -> fetchRepositoriesObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun convertJsonToRepositories(json: String): ArrayList<Repositories> {
        return Gson().fromJson(json, Array<Repositories>::class.java).toCollection(ArrayList())
    }

}