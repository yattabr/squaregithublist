package br.com.wobbu.squaregithub.di

import android.content.Context
import br.com.wobbu.squaregithub.main.RepositoriesAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule(private val context: Context) {

    @Provides
    @Singleton
    internal open fun provideContext(): Context {
        return context
    }

    @Provides
    internal open fun usersAdapter(): RepositoriesAdapter {
        return RepositoriesAdapter(context)
    }
}