package br.com.wobbu.squaregithub.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.wobbu.squaregithub.R
import br.com.wobbu.squaregithub.base.BaseApplication
import br.com.wobbu.squaregithub.base.ViewModelFactory
import br.com.wobbu.squaregithub.data.ApiResponse
import br.com.wobbu.squaregithub.data.Status
import br.com.wobbu.squaregithub.model.Repositories
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var adapter: RepositoriesAdapter
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as BaseApplication).getAppComponent().doInjection(this)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        initUI()
        initObservers()
        mainViewModel.fetchRepositories()
    }

    private fun initUI() {
        edit_search.addTextChangedListener(filterWatcher)
    }

    private fun initObservers() {
        mainViewModel.fetchRepositoriesObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> repositoriesResponse(it)
            }
        })
    }

    private fun repositoriesResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loading.visibility = View.VISIBLE
            }
            Status.SUCCESS -> {
                loading.visibility = View.GONE
                mountRecyclerView(mainViewModel.convertJsonToRepositories(apiResponse.data.toString()))
            }
            Status.ERROR -> {
                loading.visibility = View.GONE
            }
        }
    }

    private fun mountRecyclerView(items: ArrayList<Repositories>) {
        adapter.loadUsers(items)
        recycler_view.adapter = adapter
    }

    private val filterWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            adapter.filter.filter(s)
        }

    }

}
