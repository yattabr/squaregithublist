package br.com.wobbu.squaregithub.data

import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCallInterface {

    @GET(Urls.FETCH_REPOSITORIES)
    fun fetchRepositories(): Observable<JsonElement>
}