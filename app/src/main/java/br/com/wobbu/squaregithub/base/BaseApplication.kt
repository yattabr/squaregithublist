package br.com.wobbu.squaregithub.base

import android.app.Application
import android.content.Context
import br.com.wobbu.squaregithub.di.AppComponent
import br.com.wobbu.squaregithub.di.AppModule
import br.com.wobbu.squaregithub.di.DaggerAppComponent
import br.com.wobbu.squaregithub.di.UtilsModule

open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    open fun getAppComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(AppModule(this)).utilsModule(UtilsModule())
            .build()
    }

    protected override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }
}