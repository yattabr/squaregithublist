package br.com.wobbu.squaregithub.data

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}