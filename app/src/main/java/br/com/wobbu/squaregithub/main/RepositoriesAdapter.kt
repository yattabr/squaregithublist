package br.com.wobbu.squaregithub.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import br.com.wobbu.squaregithub.R
import br.com.wobbu.squaregithub.model.Repositories
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_users.view.*
import java.util.*

class RepositoriesAdapter(var providerContext: Context) :
    RecyclerView.Adapter<RepositoriesAdapter.CustomViewHolder>(), Filterable {

    private var filteredList: ArrayList<Repositories> = arrayListOf()
    private var repositoriesList: ArrayList<Repositories> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false)
        return CustomViewHolder(providerContext, view)
    }

    fun loadUsers(items: ArrayList<Repositories>) {
        filteredList = items
        repositoriesList = items
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        filteredList.sortBy { it.name }
        holder.bind(filteredList[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                filteredList = if (charString.isEmpty()) {
                    repositoriesList
                } else {
                    val list = repositoriesList.filter { item ->
                        item.name.toLowerCase(Locale.getDefault())
                            .contains(charString.toLowerCase(Locale.getDefault()))
                    }
                    list as ArrayList<Repositories>
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results!!.values as ArrayList<Repositories>
                notifyDataSetChanged()
            }

        }
    }

    class CustomViewHolder(var context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(item: Repositories) {
            itemView.txt_name.text = item.name
            itemView.txt_description.text = item.description
            Glide.with(context).load(item.owner.avatar_url).into(itemView.img_profile)
        }
    }
}