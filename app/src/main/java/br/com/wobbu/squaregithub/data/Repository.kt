package br.com.wobbu.squaregithub.data

import com.google.gson.JsonElement
import io.reactivex.Observable


class Repository(private val apiCallInterface: ApiCallInterface) {
    fun fetchRepositories(): Observable<JsonElement> {
        return apiCallInterface.fetchRepositories()
    }
}