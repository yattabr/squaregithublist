package br.com.wobbu.squaregithub.data

class Urls {
    companion object {

        const val BASE_URL = "https://api.github.com/orgs/square/"
        const val FETCH_REPOSITORIES = "repos"
    }
}